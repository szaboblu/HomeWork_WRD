import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import { HomePage, PollPage, FinishPage } from "../pages";

export const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="polls/:pollId" element={<PollPage />} />
        <Route path="finish" element={<FinishPage />} />
      </Routes>
    </BrowserRouter>
  );
};
