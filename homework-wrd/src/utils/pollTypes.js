export const pollTypes = {
  one: "SELECT_ONE",
  more: "SELECT_MORE",
  free: "FREE_TEXT"
}