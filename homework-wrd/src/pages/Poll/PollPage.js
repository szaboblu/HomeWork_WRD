import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useLocation } from "react-router-dom";
import { Poll, Question, Button } from "../../components";

export const PollPage = () => {
  const { pathname } = useLocation();
  const {allPolls} = useSelector((state) => state.poll);
  const [currentPoll, setcurrentPoll] = useState({});

  const getCurrentPoll = () => {
    const str = pathname.split("/");
    const page = str[str.length - 1];
    setcurrentPoll(allPolls.find((poll) => poll.id.toString() === page));
  };

  useEffect(() => {
    getCurrentPoll();
    console.log(allPolls, currentPoll)
  }, ['allPolls', 'currentPoll']);

  return (
    <div>
      <h1>Poll</h1>
      <Poll poll={currentPoll}></Poll>
    </div>
  );
};
