import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { init } from "../../redux";
import polls from '../../data/poll.json'
import { Button } from "../../components";

export const HomePage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const pollIds = polls.map((poll,i) => {
      return {...poll, id:i}
    })
    dispatch(init(pollIds))
  }, []);

  const beginPoll = () => {};

  return (
    <div>
      <h1>Home</h1>
      <Button onClick={beginPoll} to='polls/0' title="Begin Poll" />
    </div>
  );
};
