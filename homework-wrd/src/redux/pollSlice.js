import { createSlice } from "@reduxjs/toolkit";

export const pollSlice = createSlice({
  name: "poll",
  initialState: {
    allPolls: [],
    answeredPolls: [],
    finished: false,
  },
  reducers: {
    init: (state, action) => {
      state.allPolls = action.payload;
      state.finished = false;
    },
    answer: (state, action) => {
      state.answeredPolls = [...state.answeredPolls, action.payload];
      state.finished =
        state.counterSliceansweredPolls.length === state.allPolls.length;
    },
    changeAnswer: (state, action) => {
      if (state.answeredPolls.length > 0) {
        state.answeredPolls = state.answeredPolls.map((poll) => {
          if (poll.id === action.payload.id) {
            return action.payload;
          }
          return poll;
        });
      } else state.answeredPolls = [...state.answeredPolls, action.payload];
      state.finished =
        state.counterSliceansweredPolls.length === state.allPolls.length;
    },
  },
});

export const { init, answer, changeAnswer } = pollSlice.actions;

export default pollSlice.reducer;
