import React, { useState, useEffect } from "react";
import { Answer } from "../Answer";
import { Button } from "../Button";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { answer, changeAnswer } from "../../redux";
import { pollTypes } from "../../utils";

export const Poll = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [selected, setSelected] = useState(null);

  const saveQuestion = () => {
    dispatch(answer(selected));
  };

  const onNext = () => {
    saveQuestion();
    navigate(`polls/${props.poll.id+1}`, {replace: true});
  };

  console.log(selected);
  return (
    <div>
      <h1>{props.poll.question}</h1>
      {props.poll.answers.map((answer) => (
        <Answer
          handleClick={() => {
            setSelected(answer);
          }}
        >
          {answer}
        </Answer>
      ))}
      <Button title="Back" />
      <Button onClick={onNext} title="Next" />
    </div>
  );
};
