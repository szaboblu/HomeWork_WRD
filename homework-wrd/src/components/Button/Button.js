import React, { useState, useEffect } from "react";
import {useNavigate} from 'react-router-dom';

export const Button = (props) => {
  const navigate = useNavigate();

  const handleClick = () => {
    navigate(props.to, {replace: true});
    props.onClick();
    if (props.to === 'polls/1')  alert("You began the poll!");
  };

  return (
    <div>
      <button onClick={handleClick}>{props.title}</button>
    </div>
  );
};
