import React, { useState, useEffect } from "react";

export const Answer = (props) => {
  return (
    <div>
      <button onClick={props.handleClick}>{props.children}</button>
    </div>
  );
};
